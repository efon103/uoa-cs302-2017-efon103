# README.MD

Appl.py
Authored by Eugene Fong (efon103)

DEPENDENCIES

* CherryPy

* PyCrypto

* Jinja2

HOW TO RUN

1. Install Python

2. Install PyCrypto

3. Install Jinja2

4. open root of project

5. run app.py

6. open localhost:10007 in browser


FOR MULTIPLE USERS
Use local or external IP with port 10007 to connect additional users to the client/server.
Tested working with 4 clients (2 lab computer, 2 wireless devices)

SUPPORTED CLIENTS: scha676, hone075 (for statuses)