"""
Appl.py - A Hybrid Social Network System
    Authored by Eugene Fong (efon103)
    Last Updated: 11/06/17
    Dependencies: CherryPy, PyCrypto, Jinja2
"""

listen_ip = "0.0.0.0"
listen_port = 10007

import cherrypy
import sqlite3
import urllib
import urllib2
import threading
import binascii
import socket
import json
import cryptor
import time
import base64
import os
import database
from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Hash import SHA256
import jinja2

#Jinja2 Setup
env = jinja2.Environment(loader=jinja2.FileSystemLoader('templates'),
                         autoescape=jinja2.select_autoescape(['html', 'xml']))

#Jinja2 filter for Epoch to Date Time
def datetimeformat(value, format='%H:%M / %d-%m-%Y'):
    return time.strftime(format, time.localtime(value))

env.filters['datetimeformat'] = datetimeformat

class MainApp(object):
    """MainApp primarily controls the functionality of the CherryPy-based server"""

    key = '150ecd12d550d05ad83f18328e536f53'        # SHA-256 Hash for Login Server                         
    db = None                                       # database
    t = None                                        # Login server report thread
    u = None                                        # Get userlist from login server
    p = None                                        # Get user status/ping thread
    local_username = None                           # Username of a valid user for pings
    
    # set static directory
    WEB_ROOT = os.getcwd() + '\\static'

    # CherryPy configuration
    _cp_config = {'tools.encode.on': True,
                  'tools.encode.encoding': 'utf-8',
                  'tools.sessions.on' : 'True',
                  'tools.staticdir.on': 'True',
                  'tools.staticdir.dir' : WEB_ROOT,
                  'tools.staticdir.index': 'templates/index.html'
                 }

    def report(self):
        """Report is a threaded process that sends a notification to the login server every 30 seconds
        by via a recursive timer """
        self.t = threading.Timer(30.0, self.report).start()
        try:
            logins = self.db.getActive()    # get users that are actively logged in (for multi user)

            # get login paramaters form the database and report to login server
            for k in logins:
                if k[6] == 1:
                    login_params = {"username": k[1],
                                    "password": k[2],
                                    "ip": k[3],
                                    "port": k[4],
                                    "location": k[5],
                                    "enc": "1"}
                    url_login = urllib.urlencode(login_params)
                    response = urllib2.urlopen("http://cs302.pythonanywhere.com/report" + "?" + url_login)
                    print "beep! " + k[0] + " reported to server!"
        # except catch for when no users have not logged on or have logged off
        except:
            print "Error no one is active"

    def pingAllUsers(self):
        """Gets users status of all the users in the database"""
        # cycles every 3 minutes
        self.p = threading.Timer(180.0, self.pingAllUsers).start()
        l = self.db.getAllUsers()
        for d in l:
            self.getUserStatus(d["username"])
        print "boop! Got user statuses!"

    def getNewUsers(self):
        """Gets a new user list from the login server"""
        # cycles every 3 minutes
        self.u = threading.Timer(180.0, self.getNewUsers).start()
        self.users()
        print "biip got new users!"

    @cherrypy.expose
    def default(self, *args, **kwargs):
        """The default page, given when we don't recognise where the request is for."""
        Page = "I don't know where you're trying to go, so have a 404 Error."
        cherrypy.response.status = 404
        return Page

    @cherrypy.expose
    def index(self):
        """Index page/screen exclusive to logged in users. Primary screen"""
        try:
            # check if session is valid
            if cherrypy.session['local_username'] is not None:
                # get jinja2 template
                tmpl = env.get_template('index.html')

                # get userlist from data base 
                userlist = self.db.getAllUsers()


                try:
                # get user details of current user and highlighted user
                    cherrypy.session['local_profile'] = self.db.getProfileFromDB(cherrypy.session['local_username'])
                    cherrypy.session['other_profile'] = self.db.getProfileFromDB(cherrypy.session['other_username'])

                    # bind current user values to jinja2 template
                    local_user = {"username": cherrypy.session['local_username'],
                                "fullname": cherrypy.session['local_profile'][1],
                                "position": cherrypy.session['local_profile'][5],
                                "location": cherrypy.session['local_profile'][3],
                                "description": cherrypy.session['local_profile'][2],
                                "image": cherrypy.session['local_profile'][4],
                                }

                    # bind highlighted user values to jinja2 template
                    other_user = {"username":  cherrypy.session['other_profile'][0],
                                "fullname":  cherrypy.session['other_profile'][1],
                                "position":  cherrypy.session['other_profile'][5],
                                "location":  cherrypy.session['other_profile'][3],
                                "description":  cherrypy.session['other_profile'][2],
                                "image":  cherrypy.session['other_profile'][4],
                                }

                    # make super dictionary to bind values to template
                    toRender = {"local_user": local_user,
                                "other_user": other_user,
                                "userlist": userlist,
                                "notification": cherrypy.session['notification'],
                                "messages": cherrypy.session['messages'],
                                "status": cherrypy.session['status']
                                }
                except TypeError:
                    cherrypy.session['local_profile'] = self.db.getProfileFromDB(cherrypy.session['local_username'])
                # bind current user values to jinja2 template
                    local_user = {"username": cherrypy.session['local_username'],
                                "fullname": cherrypy.session['local_profile'][1],
                                "position": cherrypy.session['local_profile'][5],
                                "location": cherrypy.session['local_profile'][3],
                                "description": cherrypy.session['local_profile'][2],
                                "image": cherrypy.session['local_profile'][4],
                                }

                    # bind highlighted user values to jinja2 template
                    other_user = local_user

                    # make super dictionary to bind values to template
                    toRender = {"local_user": local_user,
                                "other_user": other_user,
                                "userlist": userlist,
                                "notification": cherrypy.session['notification'],
                                "messages": cherrypy.session['messages'],
                                "status": cherrypy.session['status']
                                }
                finally:
                    return tmpl.render(toRender)

        # if no users are logged in, point to the log in template
        except KeyError:
            try:
                # create data base if none exist
                if self.db == None:
                    self.db = database.database()
                    self.db.initialiseDB()
                tmpl = env.get_template('login.html')
                # render login notifications (i.e. invalid username/password)
                toRender = {"notification": cherrypy.session['notification']}

            # if no notifications, send to blank login screen
            except KeyError:
                tmpl = env.get_template('login.html')
                toRender = {}
            return tmpl.render(toRender)

    @cherrypy.expose
    @cherrypy.tools.json_in()
    def getStatus(self):
        """getStatus returns the status of the specified user
            in: json {profile_username}
            out: json {status}"""
        try:
            data = cherrypy.request.json
            user_status = self.db.getStatusFromDB(data["profile_username"]) 
            status = {'status': str(user_status[0]),} 
            return json.dumps(status)
        except:
            print "No profile username"

    @cherrypy.expose
    def updateStatus(self, status):
        """updateStatus is called by a client to change the status of a current session user
            in: status TEXT"""
        cherrypy.session['status'] = str(status)
        self.db.putStatus(cherrypy.session['local_username'], str(status))
        raise cherrypy.HTTPRedirect('/')

    @cherrypy.expose
    def updateProfile(self, fullname, position, description, location, picture):
        """updateProfile is called by a client to change the profile of a current session user
            in: fullname, position, description, location, picture TEXT"""
        profile = self.db.getProfileFromDB(cherrypy.session['local_username'])

        # if field was not changed, keep to original
        if len(fullname) == 0:
            fullname = profile[1]
        if len(position) == 0:
            position = profile[5]
        if len(description) == 0:
            description = profile[2]
        if len(location) == 0:
            location = profile[3]
        if len(picture) == 0:
            picture = profile[4]

        self.db.putProfile(str(cherrypy.session['local_username']), str(fullname), str(position),
                           str(description), str(location), str(picture))
        raise cherrypy.HTTPRedirect('/')

    @cherrypy.tools.json_in()
    @cherrypy.expose
    def getProfile(self):
        """getProfile returns a json of the desired user to the sender
            in: json {profile_username, sender}
            out: json {fullname, description, location, picture, position}"""
        data = cherrypy.request.json
        try:
            # attempt to get user profile
            user_profile = self.db.getProfileFromDB(data['profile_username'])
            output_dict = {"fullname": user_profile[1],
                           "description": user_profile[2],
                           "location": user_profile[3],
                           "picture": user_profile[4],
                           "position": user_profile[5],
                          }
            return json.dumps(output_dict)
        except:
            # return database error if unsuccessful
            return "4"

    
    @cherrypy.expose
    def ping(self, sender):
        """ping returns a 0 if connection is ok
            in: sender  TEXT"""
        return "0"

    @cherrypy.expose
    def pingUser(self, user):
        """pingUser pings the selected user to check if connection is ok
            in: user    TEXT
            out: ping_response  TEXT"""
        try:
            url = self.db.getURL(user)      # get user URL from database
            # use a valid username as sender
            req = urllib2.Request("http://" + str(url[0]) + ":" + str(url[1]) + "/ping?sender=" + self.local_username)
            response = urllib2.urlopen(req, timeout=1)
            return str(response.read())
        except urllib2.URLError as err:
            return "3"
        except socket.timeout:
            return "5"

    @cherrypy.expose
    def getProfileAndMessages(self, user):
        """getProfileAndMessages gets the most recent copy of the user (directly from the user if possible)
            and messages with that user from the database. Used to change the highlighted user on the index
            in: user    TEXT"""
        try:
            self.getUserProfile(user)
            self.displayMessages(user)
            cherrypy.session['other_username'] = user
            # once profile and messages have been retrieved, update screen
            raise cherrypy.HTTPRedirect('/')
        except urllib2.HTTPError:
            cherrypy.session['notification'] = "Error retrieving profile from user"
            raise cherrypy.HTTPRedirect('/')

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def getUserProfile(self, user):
        """getUserProfile pings the desired user to see if connection is valid. If connection is fine, then
            an attempt to get profile is processed. Otherwise, check database for previously archived copy.
            in: user    TEXT"""
        try:
            if self.pingUser(user) == "0":
                url = self.db.getURL(user)  #get URL of user
                # package request as json
                data = json.dumps({"profile_username": user,
                                   "sender": cherrypy.session['local_username']})
                req = urllib2.Request("http://" + str(url[0]) + ":" + str(url[1]) + "/getProfile?",
                                      data, {'Content-Type': 'application/json'})

                response = urllib2.urlopen(req, timeout=1)
                profile = json.loads(response.read())

                # add profile to database
                self.db.putProfile(user, profile["fullname"], profile["position"],
                                   profile["description"], profile["location"], profile["picture"])

                # make requested user the highlighted user
                cherrypy.session['other_username'] = user
        except:
            try:    #try to get archived profile otherwise, create a temporary generic profile
                profile = self.db.getProfileFromDB(user)
                if profile[0]:
                    cherrypy.session['other_username'] = user
                else:
                    self.db.putProfile(user, user, "Position",
                                    "Description", "Location", 
                                    "http://flash.za.com/wp-content/uploads/2015/08/Generic-Profile-1600x1600.png")
            except urllib2.URLError as err:
                    self.db.putProfile(user, user, "Position",
                                    "Description", "Location", 
                                    "http://flash.za.com/wp-content/uploads/2015/08/Generic-Profile-1600x1600.png")
            else:
                print "Error"

    @cherrypy.expose
    def send(self, destination, message=None, file_send=None):
        """send controls the send function on the index page so that a user can send either message only,
            file only, or both simultaneously
            in: destination, message, file_send"""
        message_state = None    # set initial reponse states
        file_state = None       

        # check if UPI is valid
        if len(destination) != 7:
            cherrypy.session['notification'] = "Destination Invalid. Please enter a valid username"
            raise cherrypy.HTTPRedirect("/")
        else:
            # check combination
            if len(message) > 0 and len(file_send.filename) <= 0:
                message_state = self.sendMessage(destination, message)
            elif len(message) <= 0 and len(file_send.filename) > 0:
                file_state = self.sendFile(destination, file_send)
            elif len(message) > 0 and len(file_send.filename) > 0:
                message_state = self.sendMessage(destination, message)
                file_state = self.sendFile(destination, file_send)
            else:
                pass
            

            if file_state == "0":
                cherrypy.session['notification'] = "File Sent!"
            elif file_state  == "6":
                cherrypy.session['notification'] = "File too big"
            elif file_state is None:
                pass
            else:
                cherrypy.session['notification'] = "File did not arrive! Destination response " + file_state

            if message_state == "0":
                cherrypy.session['notification'] = "Message Sent!"
            elif message_state == "5":
                cherrypy.session['notification'] = "Unable to ping destination"
            elif message_state is None:
                pass
            else:
                cherrypy.session['notification'] = "Message failed to arrive! Destination response " + message_state

            if file_state == "0" and message_state == "0":
                cherrypy.session['notification'] = "Message and File successfully sent!"
            
            # update messages
            self.displayMessages(cherrypy.session['local_username'])
            raise cherrypy.HTTPRedirect("/")

    @cherrypy.expose
    @cherrypy.tools.json_in()
    def receiveFile(self):
        """receiveFile downloads and saves the file sent by the other user
        in: json  {sender destination file filename content_type stamp}"""
        # create downloads folder if it does not currently exist
        if not os.path.exists(os.getcwd() + "/Downloads/"):
            os.makedirs(os.getcwd() + "/Downloads/")

        data = cherrypy.request.json
        file_data = base64.decodestring(data['file'])

        # check if file is under 5MB
        if len(file_data) < 5000000:
            filename = data["filename"]
            file_read = open("Downloads/" + filename, 'wb')
            file_read.write(file_data)

            # create notifications and store data transfer info into database
            cherrypy.session['notification'] = data['sender'] + " sent " + \
                                               filename + " to your Downloads folder"
            self.db.putMessage(data['sender'], data['destination'],
                               data['sender'] + " sent " + filename + " to your Downloads folder",
                               data['stamp'], "in")
            return "0"
        else:
            return "6"

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def sendFile(self, destination, file_send):
        """sendFile encodes the selected file to json, encodes it and sends it to the user
            in: destination TEXT, file_send FILE"""
        url = self.db.getURL(destination)   # get URL from database
        file_read = file_send.file.read()   # read file
        file_data = base64.encodestring(file_read) # encode file to base64

        # check length of data
        if len(file_data) > 5000000:
            cherrypy.session['notification'] = "File over 5MB, please choose a smaller file!"
            return "6"
        else:
            # ping user to check if connection is valid
            if self.pingUser(destination) == '0':
                # split into parameters
                filename = str(file_send.filename).split(".")
                filetype = filename[len(filename)-1]
                stamp = time.time()

                fileparam = {"sender": cherrypy.session['local_username'],
                             "destination": destination,
                             "file": file_data, "filename": file_send.filename,
                             "content_type": filetype, "stamp": stamp}
                # package into json
                data = json.dumps(fileparam)
                req = urllib2.Request("http://" + str(url[0]) + ":" + str(url[1]) + "/receiveFile?",
                                    data, {'Content-Type': 'application/json'})
                response = urllib2.urlopen(req)

                # store file details to database
                self.db.putMessage(cherrypy.session['local_username'], destination,
                                   "You sent " + file_send.filename + " to " + destination,
                                   stamp, "out")

                return str(response.read())
            else:
                return "5"

    @cherrypy.expose
    @cherrypy.tools.json_in()
    def receiveMessage(self):
        """receiveMessage receives messages in a json format and puts it in the database
        in: json {sender destination message stamp}"""
        message = cherrypy.request.json
        self.db.putMessage(message["sender"], message["destination"],
                           message["message"], message["stamp"], "in")
        return "0"

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def sendMessage(self, destination, message):
        """sendMessage packages a message and sends it to the selected user
            in: destination, message    TEXT"""
        # get message details
        sender = cherrypy.session['local_username']
        stamp = time.time()
        messageContainer = {"sender": sender,
                            "destination": destination,
                            "message": message,
                            "stamp": stamp}
        # store message in database
        self.db.putMessage(sender, destination, message, stamp, "out")

        url = self.db.getURL(messageContainer["destination"])
        try:
            if self.pingUser(destination) == "0":   # check if connection valid
                data = json.dumps(messageContainer)
                req = urllib2.Request("http://" + str(url[0]) + ":" + str(url[1]) + "/receiveMessage?",
                                      data, {'Content-Type': 'application/json'})
                response = urllib2.urlopen(req, timeout=1)
                return str(response.read())
            else:
                return "5"
        except urllib2.HTTPError:
            return "3"

    @cherrypy.expose
    def displayMessages(self, destination):
        """displayMessages puts messages of the selected user into the active user's cherrypy session.
            Allows session user to see messages of highlighted user
            in: destination     TEXT"""
        cherrypy.session['messages'] = self.db.getMessages(cherrypy.session['local_username'],
                                                           destination)

    @cherrypy.expose
    def listAPI(self):
        """listAPI lists all valid APIs supported by the client/server"""
        return '''/listAPI\n
                  /ping [sender]\n
                  /receiveMessage [sender] [destination] [message] [stamp]\n
                  /receiveFile [sender] [destination] [file] [filename] [content_type] [stamp]\n
                  /getProfile [profile_username] [sender]\n
                  /getStatus [profile_username]\n'''

    @cherrypy.expose
    def signin(self, form_username, form_password):
        """signin manages the signin process from the webpage to authenicate user.
            in: form_username, form_password    TEXT"""
        # check lengths
        if ((len(form_username) == 0) and (len(form_password) > 0)):
            cherrypy.session['notification'] = "Please enter a username"
            raise cherrypy.HTTPRedirect('/')
        elif ((len(form_username) > 0) and (len(form_password) == 0)):
            cherrypy.session['notification'] = "Please enter a password"
            raise cherrypy.HTTPRedirect('/')
        elif ((len(form_username) == 0) and (len(form_password) == 0)):
            cherrypy.session['notification'] = "Please enter a username and password"
            raise cherrypy.HTTPRedirect('/')
        else:
            form_location = self.getIP()    # get client/server IP
            if form_location == "0" or form_location == "1":
                form_ip = socket.gethostbyname(socket.gethostname())    # get local ip if within uni network
            else:
                form_ip = urllib2.urlopen('http://ip.42.pl/raw').read() # get external ip if outside
            
            # initalise database
            self.db = database.database()
            self.db.initialiseDB()

            # set temporary highlighted user and other values to defaults
            cherrypy.session['local_username'] = form_username
            cherrypy.session['messages'] = None
            try:    # get previous status
                cherrypy.session['status'] = str(self.db.getStatusFromDB(cherrypy.session["local_username"])[0])
            except:
                cherrypy.session['status'] = "Online"

            self.local_username = form_username         #set temporary username for pings
            cherrypy.session['notification'] = "Welcome to Appl.Py, " + form_username + "!"       # print welcome
            self.encrypt(form_username, form_password, form_ip, str(listen_port), form_location)  # encrypt username and pw to server
            self.checkUserValid(form_username)                                                    # report to login server

    def checkUserValid(self, username):
        """Check their name and password and send them either to the main page,
        or back to the main login screen.
            in: username    TEXT"""
        # get error code
        error = self.authoriseUserLogin(username)
        if error == 0:
            self.report()                       # start report thread
            self.db.setSignin(username, 1)      # set as logged in

            try:        # try get existing profile and check validity
                data = self.db.getProfileFromDB(username)
                len(data[0])
            except:     # otherwise, create a temporary profile
                self.db.putProfile(username, "", "",
                                   "Please create a profile", "",
                                   "http://flash.za.com/wp-content/uploads/2015/08/Generic-Profile-1600x1600.png")

            # start threads for status and userlist
            self.p = threading.Thread(target=self.pingAllUsers, args=())
            self.p.daemon = True
            self.p.start()

            self.u = threading.Thread(target=self.getNewUsers, args=())
            self.u.daemon = True
            self.u.start()

            raise cherrypy.HTTPRedirect('/')
        elif error == 1:
            cherrypy.session['notification'] += "Missing Username and/or Password"
            raise cherrypy.HTTPRedirect('/')
        elif error == 2:
            cherrypy.session['notification'] += "Unauthenticated User"
            raise cherrypy.HTTPRedirect('/')
        elif error == 3:
            cherrypy.session['notification'] += "False IP address or location reported"
            raise cherrypy.HTTPRedirect('/')
        elif error == 4:
            cherrypy.session['notification'] += "Invalid Username"
            raise cherrypy.HTTPRedirect('/')
        elif error == 6:
            cherrypy.session['notification'] += "Decryption Failed"
            raise cherrypy.HTTPRedirect('/')
        elif error == 7:
            cherrypy.session['notification'] += "API limit reached. Please try again later"
            raise cherrypy.HTTPRedirect('/')
        elif error == 8:
            cherrypy.session['notification'] += "Invalid Field Value"
            raise cherrypy.HTTPRedirect('/')
        elif error == 9:
            cherrypy.session['notification'] += "Invalid Public Key"
            raise cherrypy.HTTPRedirect('/')
        else:
            cherrypy.session['notification'] += "Unknown Error"
            raise cherrypy.HTTPRedirect('/')

    def encrypt(self, username, password, ip, port, location):
        """Uses AES encryption to encrypt login parameters for the login server.
            Stores values into database for later retrieval.
            in: username, password, ip, port, location  TEXT"""

        cherrypy.session['username'] = cryptor.encrypt(self.key, username)
        cherrypy.session['other_username'] = username

        # hash password before encryption
        h = SHA256.new()
        h.update(password + "COMPSYS302-2017")
        password = h.hexdigest()

        cherrypy.session['password'] = cryptor.encrypt(self.key, password)
        cherrypy.session['ipv4'] = cryptor.encrypt(self.key, ip)
        cherrypy.session['port'] = cryptor.encrypt(self.key, port)
        cherrypy.session['location'] = cryptor.encrypt(self.key, location)

        login_param = {"local_username": username,
                       "username": cryptor.encrypt(self.key, username),
                       "password": cryptor.encrypt(self.key, password),
                       "ip": cryptor.encrypt(self.key, ip),
                       "port": cryptor.encrypt(self.key, port),
                       "location": cryptor.encrypt(self.key, location)}
        # store in database
        self.db.putActive(login_param, "Welcome to Appl.Py, " + username + "! Enjoy your stay!", 1)

    def getIP(self):
        """getIP returns the location according to local IP address"""
        if socket.gethostbyname(socket.gethostname()).find("10.103.") == 0:
            return "0"
        elif socket.gethostbyname(socket.gethostname()).find("172.23.") == 0:
            return "1"
        else:
            return "2"

    def authoriseUserLogin(self, username):
        """authoriseUserLogin processes the initial report to the login server to check validity.
            in: username    TEXT"""
        user = self.db.getActiveByUsername(username)    # get login parameters

        login_param = {"username": str(user[0]),
                       "password": str(user[1]),
                       "ip": str(user[2]),
                       "port": str(user[3]),
                       "location": str(user[4]),
                       "enc": "1"}

        response = urllib2.urlopen("http://cs302.pythonanywhere.com/report?" + urllib.urlencode(login_param))

        if response.read() == "0, User and IP logged":
            cherrypy.session['signin'] = 1  # sign in user if credentials valid
            self.db.setSignin(username, 1)
            return 0
        else:
            cherrypy.session['signin'] = 0
            self.db.setSignin(username, 0)
            return 1

    @cherrypy.expose
    def signout(self, username):
        """Logs the current user out, expires their session.
            in: username    TEXT"""
        print "Logging off " + username
        logoffparam = {"username": cherrypy.session["username"], "password": cherrypy.session["password"], "enc": 1}
        response = urllib2.urlopen("http://cs302.pythonanywhere.com/logoff" + "?" + urllib.urlencode(logoffparam))
        cherrypy.session['signin'] = 0
        self.db.setSignin(username, 0)
        cherrypy.lib.sessions.expire()
        cherrypy.session.pop('username')
        cherrypy.session.pop('password')
        cherrypy.session.clear()
        self.db.close()
        print "Successfully Logged Off"
        raise cherrypy.HTTPRedirect('/')

    @cherrypy.expose
    def closeServer(self, username):
        """closeServer signs out all active users and shuts down CherryPy server.
            in: username    TEXT"""
        print username + " closed the server"
        active = self.db.getActive()
        for a in active:
            print "Signing out " + str(a[0])
            self.signout(str(a[0]))
            cherrypy.engine.exit()
            exit(0)

    def getUserStatus(self, user):
        """getUserStatus retrieves the status of the selected user.
            in: user    TEXT"""
        if self.pingUser(user) == "0":      # check connection
            try:    # attempt to get status
                url = self.db.getURL(user)
                request = {"profile_username": user}
                data = json.dumps(request)
                req = urllib2.Request("http://" + str(url[0]) + ":" + str(url[1]) + "/getStatus?",
                                    data, {'Content-Type': 'application/json'})
                response = urllib2.urlopen(req, timeout=1)

                user_status = json.loads(response.read())
                self.db.putStatus(user, str(user_status.get("status")))
            except urllib2.HTTPError as err:    # ping is valid, but user does not have getStatus
                self.db.putStatus(user, "Online")
                print str(user) + " does not have getStatus", err.reason
            except urllib2.URLError as err:    # ping is valid, but something is funny
                self.db.putStatus(user, "Online")
                print "Some other error happened for status:", err.reason
            except ValueError:                 # getStatus is valid, but returned object is not JSON
                self.db.putStatus(user, "Online")
                print "ValueError: No JSON object could be decoded"
            except:                            # IDK aye
                self.db.putStatus(user, "Online")
                print "IDK aye"
        else:
            self.db.putStatus(user, "Offline") # if ping is invalid, mark user as offline

    @cherrypy.expose
    def users(self):
        """users gets and update the details of each logged user from the report server"""
        try:
            users = self.db.getActive()     # get credentials of session user
            getlistparam = {"username": users[0][1],
                            "password": users[0][2],
                            "json": cryptor.encrypt(self.key, "1"),
                            "enc": 1}

            url_getlist = urllib.urlencode(getlistparam)
            response = urllib2.urlopen("http://cs302.pythonanywhere.com/getList?" + url_getlist)
            userlist = json.loads(response.read())

            for d in userlist:
                # if user does not exist in database, create new user
                if self.db.getUserlist(userlist[d]["username"]) is None:
                    self.db.putUserlist(userlist[d]["username"],
                                        userlist[d]["ip"],
                                        userlist[d]["port"],
                                        userlist[d]["location"],
                                        userlist[d]["lastLogin"],
                                        "Online",)
                # otherwise, just update necessary info
                else:
                    self.db.updateUserlist(userlist[d]["username"],
                                        userlist[d]["ip"],
                                        userlist[d]["port"],
                                        userlist[d]["location"],
                                        userlist[d]["lastLogin"],)
        except:
            print "no session users logged in"

def runMainApp():
    # Create an instance of MainApp and tell Cherrypy to send all requests under / to it. (ie all of them)
    
    cherrypy.tree.mount(MainApp(), "/")

    # Tell Cherrypy to listen for connections on the configured address and port.

    cherrypy.config.update({'server.socket_host': listen_ip,
                            'server.socket_port': listen_port,
                            'engine.autoreload.on': True,
                           })

    print "========================="
    print "University of Auckland"
    print "COMPSYS302 - Software Design Application"
    print "App.py"
    print "Authoured by Eugene Fong (efon103)"
    print "Please open 'localhost:10007' in your browser to access application"
    print "========================================"

    # Start the web server
    cherrypy.engine.start()

    # And stop doing anything else. Let the web server take over.
    cherrypy.engine.block()

#Run the function to start everything
runMainApp()
