"""Cryptor.py
    AES Encryption
    Authored by Eugene Fong (efon103)
    Last Modified: 6/6/2017
"""

from Crypto.Cipher import AES
from Crypto import Random
import binascii
import math

def encrypt(key, data):
    iv = Random.new().read(16)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    enc = iv + cipher.encrypt(str(data).ljust(int(math.ceil(len(data) / 16.0) * 16)))
    return binascii.hexlify(enc)

def decrypt(key, enc):
    enc = binascii.unhexlify(enc)
    iv = enc[:16]
    cipher = AES.new(key, AES.MODE_CBC, iv)
    return cipher.decrypt(enc[16:]).rstrip(" ")
