"""
database.py
    database manages all the database requests
    Authored by Eugene Fong (efon103)
    Last Modified: 11/06/2017
    """

import sqlite3
import uuid
import urllib
import urllib2
import binascii
import math
import json
import cryptor
import time
import threading

class database:
    c = None
    conn = None
    lock = threading.Lock()     # thread lock to ensure only one request at a time

    def initialiseDB(self):
        """initialiseDB sets up database and tables if currently not existing"""
        self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
        self.c = self.conn.cursor()

        self.c.execute('''CREATE TABLE IF NOT EXISTS "main"."profiles"(
                                "username" TEXT PRIMARY KEY  NOT NULL ,
                                "fullname" TEXT, "description" TEXT, 
                                "location" TEXT, "picture" TEXT, 
                                "position" TEXT)''')
        self.c.execute('''CREATE TABLE IF NOT EXISTS "main"."userlist"(
                                "username" TEXT PRIMARY KEY  NOT NULL , 
                                "ip" TEXT, "port" TEXT, "location" TEXT, 
                                "lastLogin" TEXT, "publicKey" TEXT, "status" TEXT)''')
        self.c.execute('''CREATE TABLE IF NOT EXISTS "main"."messages"(
                                "id" TEXT PRIMARY KEY  NOT NULL  DEFAULT (null),
                                "sender" TEXT NOT NULL ,"destination" TEXT NOT NULL,
                                "message" TEXT NOT NULL ,"stamp" FLOAT NOT NULL DEFAULT (null),
                                "markdown" BOOL,"encryption" INTEGER,"hashing" INTEGER,
                                "hash" TEXT,"decryptionKey" TEXT,"direction" TEXT)''')
        self.c.execute('''CREATE TABLE IF NOT EXISTS "main"."active" (
                        "local_username" TEXT PRIMARY KEY  NOT NULL ,
                        "username" TEXT, 
                        "password" TEXT, 
                        "ip" TEXT, 
                        "port" TEXT, 
                        "location" TEXT,
                        "notification" TEXT, 
                        "signin" BOOL)''')
        self.conn.commit()
        self.conn.close()

    def putActive(self, login_param, notification, signin):
        """putActive puts details of active users into the database"""
        try:
            self.lock.acquire(True)
            self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
            self.c = self.conn.cursor()
            self.c.execute('''INSERT OR REPLACE INTO active (
                           local_username, username, password, ip, port, location, notification, signin)
                           VALUES (?,?,?,?,?,?,?,?)''',
                           (login_param["local_username"],
                            login_param["username"],
                            login_param["password"],
                            login_param["ip"],
                            login_param["port"],
                            login_param["location"],
                            notification,
                            signin, ))
            self.conn.commit()
            self.conn.close()
        finally:
            self.lock.release()

    def getActive(self):
        """putActive retrieves details of active users from the database"""
        try:
            self.lock.acquire(True)
            self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
            self.c = self.conn.cursor()
            self.c.execute('''SELECT local_username, username, password, ip, port, location, signin
                        FROM active WHERE signin = "1"''', )
            data = self.c.fetchall()
            self.conn.close()
        finally:
            self.lock.release()
        return data

    def getActiveByUsername(self, username):
        """getActiveByUsername retrieves details of a specific user from active users"""
        self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
        self.c = self.conn.cursor()
        self.c.execute('''SELECT username, password, ip, port, location
                       FROM active WHERE local_username = ?''', (str(username), ))
        data = self.c.fetchone()
        self.conn.close()
        return data

    def getNotification(self, username):
        """getNotification gets the notification of a specific user"""
        self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
        self.c = self.conn.cursor()
        self.c.execute('''SELECT notification
                       FROM active WHERE local_username = ?''', (str(username), ))
        data = self.c.fetchone()
        self.conn.close()
        return data

    def updateNotification(self, username, notification):
        """updateNotification updates the notification of a specific user"""
        self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
        self.c = self.conn.cursor()
        self.c.execute('''UPDATE active SET notification = ? WHERE local_username = ?''',
                       (notification, username, ))
        self.conn.commit()
        self.conn.close()

    def getSignin(self):
        """getSignin gets the sign in status of a specific user"""
        self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
        self.c = self.conn.cursor()
        self.c.execute('''SELECT signin
                       FROM active WHERE signin = 1''', )
        data = self.c.fetchone()
        self.conn.close()
        return data

    def setSignin(self, username, signin):
        """setSignin sets the sign in status of a specific user"""
        self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
        self.c = self.conn.cursor()
        self.c.execute('''UPDATE active SET signin = ? WHERE local_username = ?''',
                       (signin, username, ))
        self.conn.commit()
        self.conn.close()

    def getURL(self, username):
        """getURL gets the url of a specific user"""
        try:
            self.lock.acquire(True)
            self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
            self.c = self.conn.cursor()
            self.c.execute('''SELECT ip, port FROM userlist WHERE username = ?''', (str(username), ))
            data = self.c.fetchone()
            self.conn.close()
        finally:
            self.lock.release()
        return data

    def putMessage(self, sender, destination, message, stamp, direction):
        """putMessage puts a message in the messages table"""
        try:
            self.lock.acquire(True)
            self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
            self.c = self.conn.cursor()
            self.c.execute('''INSERT OR REPLACE INTO messages (
                            id, sender, destination, message, stamp, direction)
                            VALUES (?,?,?,?,?,?)''',
                        (str(uuid.uuid4()), sender, destination,
                            message, stamp, str(direction), ))
            self.conn.commit()
            self.conn.close()
        finally:
            self.lock.release()

    def getMessages(self, sender, destination):
        """getMessage fetches a message in the messages table"""
        self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
        self.c = self.conn.cursor()
        getAll = self.c.execute('''SELECT sender, destination, message, stamp FROM messages
                                WHERE (sender = ? AND destination = ?) OR (sender = ? AND destination = ?)
                                ORDER BY stamp DESC''',
                                (sender, destination, destination, sender))
        colname = [d[0] for d in getAll.description]
        data = [dict(zip(colname, r)) for r in getAll.fetchall()]
        self.conn.close()
        return data

    def putUserlist(self, username, ip, port, location, lastLogin, status):
        """putUserlist adds a user to the userlist table"""
        try:
            self.lock.acquire(True)
            self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
            self.c = self.conn.cursor()
            self.c.execute('''INSERT OR REPLACE INTO userlist (
                           username, ip, port, location, lastLogin, status)
                           VALUES (?,?,?,?,?,?)''',
                           (username,
                            ip,
                            port,
                            location,
                            lastLogin,
                            status, ))
            self.conn.commit()
            self.conn.close()
        finally:
            self.lock.release()

    def updateUserlist(self, username, ip, port, location, lastLogin):
        """updateUserlist updates a user in the userlist table"""
        try:
            self.lock.acquire(True)
            self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
            self.c = self.conn.cursor()
            self.c.execute('''UPDATE userlist SET
                           ip = ?,
                           port = ?,
                           location = ?,
                           lastLogin= ?
                           WHERE username = ?''',
                           (ip, port, location, lastLogin, username, ))
            self.conn.commit()
            self.conn.close()
        finally:
            self.lock.release()

    def getUserlist(self, username):
        """getUserlist gets a user from the userlist table"""
        try:
            self.lock.acquire(True)
            self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
            self.c = self.conn.cursor()
            self.c.execute("SELECT * FROM userlist WHERE username=?", (username,))
            data = self.c.fetchone()
            self.conn.close()
        finally:
            self.lock.release()
        return data

    def getAllUsers(self):
        """getAllUser gets a user from the userlist table"""
        try:
            self.lock.acquire(True)
            self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
            self.c = self.conn.cursor()
            getAll = self.c.execute("SELECT * FROM userlist")
            colname = [d[0] for d in getAll.description]
            result_list = [dict(zip(colname, r)) for r in getAll.fetchall()]
        finally:
            self.lock.release()
        return result_list


    def getStatusFromDB(self, username):
        """getStatusFromDB gets a user's status from the userlist table"""
        try:
            self.lock.acquire(True)
            self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
            self.c = self.conn.cursor()
            self.c.execute("SELECT status FROM userlist WHERE username=?",
                        (username,))
            data = self.c.fetchone()
            self.conn.close()
        finally:
            self.lock.release()
        return data
    
    def putStatus(self, username, status):
        """putStatusFromDB puts a user's status into the userlist table"""
        try:
            self.lock.acquire(True)
            self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
            self.c = self.conn.cursor()
            self.c.execute("UPDATE userlist SET status = ? WHERE username = ?",
                        (status, username))
            self.conn.commit()
            self.conn.close()
        finally:
            self.lock.release()

    def putProfile(self, username, fullname, position, description, location, picture):
        """putProfile puts a user's profile into the database"""
        try:
            self.lock.acquire(True)
            self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
            self.c = self.conn.cursor()
            self.c.execute('''INSERT OR REPLACE INTO profiles (
                            username, fullname, position, description, location, picture)
                            VALUES (?,?,?,?,?,?)''',
                        (username, fullname, position,
                            description, location, picture, ))
            self.conn.commit()
            self.conn.close()
        finally:
            self.lock.release()

    def getProfileFromDB(self, username):
        """getProfileFromDB gets a user's profile from the database"""
        try:
            self.lock.acquire(True)
            self.conn = sqlite3.connect('app.sqlite', check_same_thread=False)
            self.c = self.conn.cursor()
            self.c.execute("SELECT * FROM profiles WHERE username=?", (username,))
            data = self.c.fetchone()
            self.conn.close()
        finally:
            self.lock.release()
        return data

    def close(self):
        self.c = None
        self.conn.close()